#!/bin/bash
# Open up tmux sessions for all servers and tile the windows

# list of apache servers
SRVLSTA="hostx hosty hostz"

# list of debian servers
SRVLSTD="host1 host2 host3"

# list of CentOS servers
SRVLSTC="host4 host5 host6"

# list of desktops
SRVLSTT="desktop1 desktop2 desktop3"

# used later if needed to spawn new session
SESSTAG=""

function usage {
  echo 'usage: tmas.sh [-N] [<hostgroup>] [-X <additional hosts>]
               tmas.sh [[-h|--help]|-L]
  hostgroups (choose one of):
    -a    apache servers
    -d    debian servers
    -c    centos servers
    -t    linux desktops
    -X    connect to additional hosts as provided (must be last arguments
            provided in command)

  -N    force a new session
  -h    show this help
  -L    list hosts
'
  return 0;
}

function listhosts {
  echo Apache servers: $SRVLSTA
  echo
  echo Debian hosts: $SRVLSTD
  echo
  echo CentOS hosts: $SRVLSTC
  echo
  echo Linux desktops: $SRVLSTT
  echo
  return 0;
}

while true; do
 case $1 in
     -a)
      srvlst=$SRVLSTA
      shift
      ;;
    -d)
      srvlst=$SRVLSTD
      shift
      ;;
    -c)
      srvlst=$SRVLSTC
      shift
      ;;
    -t)
      srvlst=$SRVLSTT
      shift
      ;;
    -N)
      SESSTAG=`tmux list-sessions|wc -l`
      shift
      ;;
    -h|--help)
      usage
      exit 0
      ;;
    -u)
      srvlst=$SRVLST
      shift
      ;;
    -L)
      listhosts
      exit 0
      ;;
    -X)
      shift
      srvlst="$srvlst $@"
      break
      ;;
    "")
      break
      ;;
    *)
      usage
      exit 0
      ;;
  esac;
done;

if [[ $srvlst =~ " -" ]]; then
    echo Custom host list must be last in provided arguments
    echo
    usage
    exit 1
fi

if [ "$srvlst" == "" ]; then
    srvlst=$SRVLSTD;
fi

SESSION=updates${SESSTAG}
SSH_USER=$USER
WINDOW=tmas
# synchronize panes: 1=yes, 0=no
SYNC=1
SERVERS=$srvlst

echo WINDOW is $WINDOW
open_windows() {
    # true if this is a brand new session
    new_session=$1

    tmux list-windows -t "$SESSION" | grep "$WINDOW" > /dev/null
    if [ $? -eq 0 ]; then
        echo "Window '$SESSION:$WINDOW' already exists. Aborting."
        return
    fi

    # if this is a new session then there is no point in creating a new window
    if [ -n "$new_session" ]; then
        # this is a brand new session with only 1 window, so just rename it
        echo starting new session
        tmux rename-window  -t "$SESSION:0" "$WINDOW"
    else
        # create new window in this pre-existing session
        echo creating new window in existing session
        tmux new-window    -t "$SESSION" -a -n "$WINDOW"
    fi

    # split window and echo servers to each pane
    # sending keys gives you a chance to decide when to type in password
    for server in $SERVERS; do
        echo splitting window
        tmux split-window  -t "$SESSION:$WINDOW" -h
        echo ssh to $server
        tmux send-keys     -t "$SESSION:$WINDOW" "ssh $server"
        # split -h seems to stop at 5 unless layout is changed
        echo updating layout
        tmux select-layout -t "$SESSION:$WINDOW" tiled
    done

    echo final layout refresh
    tmux kill-pane     -t "$SESSION:$WINDOW.0"     # remove the original pane
    tmux select-pane   -t "$SESSION:$WINDOW.0"     # select the first pane
    tmux select-layout -t "$SESSION:$WINDOW" tiled # tile panes evenly

    # synchronize input to all panes unless turned off
    if [ "$SYNC" -gt 0 ]; then
        echo enabling pane sync
        tmux set-window-option -t "$SESSION:$WINDOW" synchronize-panes on
    fi
}

# does this session already exist?
echo cheking for existing session
tmux has-session -t "$SESSION"
if [ $? -ne 0 ]; then
    if [ -n "$TMUX" ]; then
        echo tmux session already attached
        # we are attached to a session of another name
        attached=`tmux list-sessions | grep attached | cut -d: -f1`
        echo "Creating new session '$SESSION' while attached to '$attached' is not well supported."
        exit 1
    else
        # session does not exist. create it.
        # this won't work if we are attached. even if the session name is different
        # the hack is to remove $TMUX temporarily
        echo creating new tmux session
        tmux new-session -d -s "$SESSION"
        split_only=1
    fi
fi
# time to open a window and chop it up
open_windows $split_only

if [ -z "$TMUX" ]; then
    # now let's attach to the session if we aren't already attached
    # to some session
    echo attempting to attach to session
    tmux attach -t "$SESSION"
fi
