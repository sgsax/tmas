# tmas

## TMAS: TMux All Servers

Bash shell script that takes a list of hosts and creates a tiled display for tmux sessions and starts an ssh connection to those hosts. Turns on pane sync by default so you can enter the same command on all windows simultaneously.

To customize, edit the `SRVLST*` variables with your own hosts. If you add new lists, you also need to add entries to the case statement where commandline flags are interpreted.

## usage

```
usage: tmas.sh [-N] [<hostgroup>] [-X <additional hosts>]
               tmas.sh [[-h|--help]|-L]
  hostgroups (choose one of):
    -a    apache servers
    -d    debian servers
    -c    centos servers
    -t    linux desktops
    -X    connect to additional hosts as provided (must be last arguments
            provided in command)

  -N    force a new session
  -h    show this help
  -L    list hosts
```
